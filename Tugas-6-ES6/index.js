// soal 1
const luas = (panjang, luas) => {
    return panjang * luas;
}

const keliling = (panjang, luas) => {
    return 2 * (panjang + luas);
}

console.log("Luas = " + luas(4,2));
console.log("Keliling = " + keliling(4, 2));

// soal 2
const literal = (firstName, lastName) => {
    return {
        firstName: firstName,
        lastName: lastName,
        fullname : () => {
            console.log(firstName + " " + lastName)
        }
    }
}

literal("William", "Imoh").fullname();

// soal 3
const newObject = {
    firstName: "Muhammad",
    lastName: "Iqbal Mubarok",
    address: "Jalan Ranamanyar",
    hobby: "playing football",
}

const {firstName, lastName, address, hobby} = newObject;
console.log(firstName, lastName, address, hobby);

// soal 4
const west = ["Will", "Chris", "Sam", "Holly"];
const east = ["Gill", "Brian", "Noel", "Maggie"];
let combined = [...west, ...east];
// Driver Code
console.log(combined);

// soal 5
const planet = "earth" 
const view = "glass" 
const before = `Lorem ${view} dolor sit amet, consectetur adipiscing elit, ${planet}`;