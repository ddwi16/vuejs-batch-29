// soal 1
var nilai = 75;

// . . . jawaban soal 1
if(nilai >= 85) 
{
    console.log("indeks nilai A");
} 
else if(nilai >= 75 && nilai < 85) 
{
    console.log("indeks nilai B");
} 
else if(nilai >= 65 && nilai < 75) 
{
    console.log("indeks nilai C");
} 
else if(nilai >= 55 && nilai < 65) 
{
    console.log("indeks nilai D");
} 
else if(nilai < 55) 
{
    console.log("indeks nilai E");
}

// soal 2
var tanggal = 16;
var bulan = 11;
var tahun = 1999;

//  . . . jawaban soal 2
switch (bulan) {
    case 1:
        console.log("16 Januari 1999");
        break;
    case 2:
        console.log("16 Februari 1999");
        break;
    case 3:
        console.log("16 Maret 1999");
        break;
    case 4:
        console.log("16 April 1999");
        break;
    case 5:
        console.log("16 Mei 1999");
        break;
    case 6:
        console.log("16 Juni 1999");
        break;
    case 7:
        console.log("16 Juli 1999");
        break;
    case 8:
        console.log("16 Agustus 1999");
        break;
    case 9:
        console.log("16 September 1999");
        break;
    case 10:
        console.log("16 Oktober 1999");
        break;
    case 11:
        console.log("16 November 1999");
        break;
    case 12:
        console.log("16 Desember 1999");
        break;
    default:
        break;
}

// soal 3
var n = 5;
var output = "#";

// . . . jawaban soal 3
for(var i = 0; i <= n; i++){
    for(var j = 1; j <= i; j++) {
        process.stdout.write("#");
    }
    console.log("\n");
}

// soal 4
var m = 7;
var i = 1;

//  . . . jawaban soal 4
for(i; i <=m; i++) {
    if(i % 3 == 1) {
        console.log(i + " - I love programming");
    } else if(i % 3 == 2) {
        console.log(i + " - I love Javascript");
    } else if(i % 3 == 0) {
        console.log(i + " - I love VueJS");
        console.log("===");
    }
}