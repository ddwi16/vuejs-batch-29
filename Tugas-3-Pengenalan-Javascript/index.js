// Soal 1
var pertama = "saya sangat senang hari ini";
var kedua = "belajar javascript itu keren";

var kataPertama = pertama.substring(0, 5);
var kataKedua = pertama.substring(12, 19);
var kataKetiga = kedua.substring(0, 7);
var kataKeempat = kedua.substring(7, 18);

console.log("Soal 1");
console.log(kataPertama.concat(kataKedua, kataKetiga, kataKeempat.toUpperCase()));
console.log("\n");

// Soal 2
var kataPertama = "10";
var kataKedua = "2";
var kataKetiga = "4";
var kataKeempat = "6";

var numberKata1 = Number(kataPertama);
var numberKata2 = Number(kataKedua);
var numberKata3 = Number(kataKetiga);
var numberKata4 = Number(kataKeempat);

function OperasiHitung(numberKata1, numberKata2, numberKata3, numberKata4) {
    return (numberKata1 + numberKata2) * (numberKata3 + numberKata4);
}

console.log("Soal 2");
console.log(OperasiHitung(numberKata1, numberKata2, numberKata3, numberKata4));
console.log("\n");

// Soal 3
var kalimat = 'wah javascript itu keren sekali'; 

var kataPertama = kalimat.substring(0, 3); 
var kataKedua = kalimat.substring(4, 14); 
var kataKetiga = kalimat.substring(15, 18); 
var kataKeempat = kalimat.substring(19, 24); 
var kataKelima = kalimat.substring(25, 33); 

console.log("Soal 3");
console.log('Kata Pertama: ' + kataPertama); 
console.log('Kata Kedua: ' + kataKedua); 
console.log('Kata Ketiga: ' + kataKetiga); 
console.log('Kata Keempat: ' + kataKeempat); 
console.log('Kata Kelima: ' + kataKelima);
console.log("\n");