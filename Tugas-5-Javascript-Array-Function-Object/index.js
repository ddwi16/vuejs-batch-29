// soal 1
var daftarHewan = ["2. Komodo", "5. Buaya", "3. Cicak", "4. Ular", "1. Tokek"];
daftarHewan.sort(); // ..... jawaban soal 1
console.log(daftarHewan + "\n");

// soal 2
function Introduce(data) {
    return `Nama saya ${data.name}, umur saya ${data.age} tahun, alamat saya di ${data.address}, dan saya punya hobby yaitu ${data.hobby}`;
} // ..... jawaban soal 2
 
var data = {
    name : "John", 
    age : 30, 
    address : "Jalan Pelesiran", 
    hobby : "Gaming" 
};
 
var perkenalan = Introduce(data);
console.log(perkenalan + "\n"); 
// Menampilkan "Nama saya John, umur saya 30 tahun, alamat saya di Jalan Pelesiran, dan saya punya hobby yaitu Gaming"

// soal 3
function hitung_huruf_vokal(name) {
    var list_vokal = 'aiueoAIUEO';
    var hitung_vokal = 0;

    for(var i = 0; i < name.length; i++) {
        if(list_vokal.indexOf(name[i]) !== -1) {
            hitung_vokal += 1;
        }
    }

    return hitung_vokal;
} // ..... jawaban soal 3

var hitung_1 = hitung_huruf_vokal("Muhammad");

var hitung_2 = hitung_huruf_vokal("Iqbal");

console.log(hitung_1 , hitung_2, "\n"); // 3 2

// soal 4
function hitung(number) {
    var output = -2;

    for (var i = 0; i < number; i++) {
        output += 2;
    }

    return output;
} // ..... jawaban soal 4

console.log( hitung(0) ); // -2
console.log( hitung(1) ); // 0
console.log( hitung(2) ); // 2
console.log( hitung(3) ); // 4
console.log( hitung(5) ); // 8