// soal 1
function next_date(tanggal, bulan, tahun) {
    var new_tanggal = tanggal + 1;
    var set_tanggal = 0;
    
    if(new_tanggal > set_tanggal) {
        new_tanggal = 1;
        bulan += 1;
    }

    if(bulan > 12) {
        bulan = 1;
        tahun += 1;
    }

    switch (bulan) {
        case 1:
            bulan = "Januari";
            set_tanggal = 31;
            break;
        case 2:
            bulan = "Februari";
            if(tahun % 4 == 0) {
                set_tanggal = 28;
            }
            else {
                set_tanggal = 29;
            }
            break;
        case 3:
            bulan = "Maret";
            set_tanggal = 31;
            break;
        case 4:
            bulan = "April";
            set_tanggal = 30;
            break;
        case 5:
            bulan = "Mei";
            set_tanggal = 31;
            break;
        case 6:
            bulan = "Juni";
            set_tanggal = 30;
            break;
        case 7:
            bulan = "Juli";
            set_tanggal = 31;
            break;
        case 8:
            bulan = "Agustus";
            set_tanggal = 30;
            break;
        case 9:
            bulan = "September";
            set_tanggal = 31;
            break;
        case 10:
            bulan = "Oktober";
            set_tanggal = 30;
            break;
        case 11:
            bulan = "November";
            set_tanggal = 31;
            break;
        case 12:
            bulan = "Desember";
            set_tanggal = 30;
            break;                   
        default:
            break;
    }

    return console.log(new_tanggal, bulan, tahun);
}

var tanggal = 29
var bulan = 2
var tahun = 2020

next_date(tanggal , bulan , tahun ) // output : 1 Maret 2020

var tanggal = 28
var bulan = 2
var tahun = 2021

next_date(tanggal , bulan , tahun ) // output : 1 Maret 2021

var tanggal = 31
var bulan = 12
var tahun = 2020

next_date(tanggal , bulan , tahun ) // output : 1 Januari 2021

// soal 2
function jumlah_kata(kata) { 
    var hitung_kata = 0;
    var kalimat = kata.trim();
    
    for (var i = 0; i < kalimat.length; i++) {
        var karakter = kalimat[i];

        if (karakter == " ") {
            hitung_kata += 1;
        }
    }

    hitung_kata += 1;
    return console.log(hitung_kata);
}

var kalimat_1 = " Halo nama saya Muhammad Iqbal Mubarok ";
var kalimat_2 = " Saya Iqbal";
var kalimat_3 = " Saya Muhammad Iqbal Mubarok ";

jumlah_kata(kalimat_1); // 6
jumlah_kata(kalimat_2); // 2
jumlah_kata(kalimat_3); // 4
